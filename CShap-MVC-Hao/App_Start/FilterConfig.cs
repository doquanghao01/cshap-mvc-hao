﻿using System.Web;
using System.Web.Mvc;

namespace CShap_MVC_Hao
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
